from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'NantiSchool.views.home', name='home'),

    url(r'^register/', 'UsersExtended.views.register', name='register'),
    url(r'^login/', 'UsersExtended.views.login_custom', name='login'),
    url(r'^logout/', 'UsersExtended.views.logout_custom', name='logout'),
    url(r'^adduser/', 'UsersExtended.views.adduser', name='adduser'),
    url(r'^edituser/(\d+)', 'UsersExtended.views.edituser', name='edituser'),
    url(r'^edituser/', 'UsersExtended.views.listusers', name='listusers'),
    url(r'^updateuser/', 'UsersExtended.views.updateuser', name='updateuser'),
    url(r'^deluser/(\d+)', 'UsersExtended.views.deluser', name='deluser'),
    #url(r'^NantiSchool/', include('NantiSchool.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^Exams/$', 'Exams.views.list', name='list_exams'),
    url(r'^Exams/Take/(?P<exam_id>\d+)/$', 'Exams.views.take', name='take_exams'),
    url(r'^Exams/AnswerQuestion/', 'Exams.views.answer'),

    #CRUD
    url(r'^Exams/Create', 'Exams.views.create', name='create_exams'),
    url(r'^Exams/Edit/(?P<exam_id>\d+)/$', 'Exams.views.edit', name='edit_exams'),
    url(r'^Exams/Delete/(?P<exam_id>\d+)/$', 'Exams.views.delete', name="delete_exams"),
    url(r'^Exams/EditExamData/', 'Exams.views.edit_exam_data'),

    #Question CRUD
    url(r'^Exams/AddQuestion/', 'Exams.views.add_question'),
    url(r'^Exams/EditQuestion/', 'Exams.views.edit_question'),
    url(r'^Exams/DeleteQuestion/', 'Exams.views.delete_question'),

    #Option CRUD
    url(r'^Exams/AddOption/', 'Exams.views.add_option'),
    url(r'^Exams/EditOption/', 'Exams.views.edit_option'),
    url(r'^Exams/DeleteOption/', 'Exams.views.delete_option'),
    url(r'^Exams/EditCheckbox/', 'Exams.views.edit_checkbox'),

    url(r'^admin/', include(admin.site.urls)),
)

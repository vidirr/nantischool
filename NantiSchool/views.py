from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from UsersExtended.models import UserProfile

@login_required
def home(request):
	return redirect('list_exams')


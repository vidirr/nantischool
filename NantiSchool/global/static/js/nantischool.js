$(document).ready(function() {

	$('#examName').blur(function() {
		$('#nameLoader').toggle();
		EditExamData('#examName', '#nameLoader');
	});
	$('#examCreationDate').blur(function() {
		$('#creationLoader').toggle();
		EditExamData('#examCreationDate', '#creationLoader');
	});
	$('#examDueDate').blur(function() {
		$('#dueLoader').toggle();
		EditExamData('#examDueDate', '#dueLoader');
	});
});


function EditExamData(inputid, wheelid) {
	$("#savingMessage").html('Saving ..');
	$.ajax({
		type: "POST",
		url: "/Exams/EditExamData/",
		data: { exam_id: $('#examId').val(),
				name: $('#examName').val(),
				creation_date: $("#examCreationDate").val(),
				due_date: $("#examDueDate").val()
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				$("#savingMessage").html('Saved!');
				$(inputid).css('background-color', '#7AE969');
				$(inputid).animate({ 'background-color' : 'white'}, 2000);
				setTimeout(function() {
					$(wheelid).fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {

			$(wheelid).toggle();
		}
	})
}

function AddQuestion() {

	$.ajax({
		type: "POST",
		url: "/Exams/AddQuestion/",
		data: { exam_id: $('#examId').val(),
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {

		},
		error: function() {

		}
	})
}
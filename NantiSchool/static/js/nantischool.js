

$(document).ready(function() {

	$('#examName').blur(function() {
		$('#nameLoader').toggle();
		$('#titleLoader').toggle();
		EditExamData('#examName', '#nameLoader');
	});

	$('#examCreationDate').blur(function() {
		$('#creationLoader').toggle();
		$('#titleLoader').toggle();
		EditExamData('#examCreationDate', '#creationLoader');
	});

	$('#examDueDate').blur(function() {
		$('#dueLoader').toggle();
		$('#titleLoader').toggle();
		EditExamData('#examDueDate', '#dueLoader');
	});

    $('#examVideo').blur(function() {
        $('#videoLoader').toggle();
        $('#titleLoader').toggle();
        EditExamData('#examVideo', '#videoLoader');
    });

	$('.addQuestion').click(function() {
		$("#noQuestions").hide();
		$.ajax({
				type: "POST",
				url: "/Exams/AddQuestion/",
				data: { exam_id: $('#examId').val(),
				 },
				contentType: "application/json; charset-utf8",
				dataType: "json",
				success: function(data) {
					id = [data];
				},
				error: function() {

				}
			}).complete(function() {
				$("#QuestionTmpl").tmpl(id).appendTo('#questionList');
			});	
	});

	$('#questionList').on({
		
			blur: function() {
				$('#Loader' + this.id).toggle();
				$('#titleLoader').toggle();
				EditQuestion(this.id);
			}
		}, '.questionInput');
	});

	$('#questionList').on({

			click: function() {
				$('#Loader' + this.id).toggle();
				$('#titleLoader').toggle();
				DeleteQuestion(this.value);
			}
		}, '.deleteButton'
	);

	$("#questionList").on({

		click: function() {
		value = this.value;
		$("#noQuestions").hide();
			var id;
			$.ajax({
				type: "POST",
				url: "/Exams/AddOption/",
				data: { question_id: value,
				 },
				contentType: "application/json; charset-utf8",
				dataType: "json",
				success: function(data) {
					id = [data];
				},
				error: function() {

				}
				}).complete(function() {
				$("#OptionTmpl").tmpl(id).appendTo('#OptionList-' + value);
			});	
	
		}
	}, '.btn-primary');

	$('#questionList').on({
	
		blur: function() {
			$('#optionLoader-' + this.id).toggle();
			$('#titleLoader').toggle();
			EditOption(this.id);
		}
	}, '.OptionInput');
	
	$("#takeExam").on({

		click: function() {
			$("titleLoader").toggle();
			$.ajax({
					type: "POST",
					url: "/Exams/AnswerQuestion/",
					data: { 
							option_id: this.id,
							student_takes: $("#takes_exam").val(), 
							question_id: this.name
					 },
					contentType: "application/json; charset-utf8",
					dataType: "json",
					
					success: function(data) {
						setTimeout(function() {
							$("#savingMessage").html('Saved!');
							setTimeout(function() {
								$('#titleLoader').fadeOut('slow');
								$("#savingMessage").html("");
							}, 1000);
						}, 1500);

					},
					error: function() {
						$("#savingMessage").html("Error!");
						setTimeout(function() {
							$('#titleLoader').fadeOut('fast');
							$("#savingMessage").html("");
							}, 2000);
					}
				});		
		}

	}, 'input[type=radio]');

	$('#questionList').on({

			click: function() {
				$('#optionLoader-' + this.id).toggle();
				$('#titleLoader').toggle();
				DeleteOption(this.value);
			}
		}, '.OptiondeleteButton'
	);

	$('#questionList').on({

			click: function() {
				var id = this.id;
				$('#CheckLoader-' + this.id).toggle();
				$('#titleLoader').toggle();
				$("#savingMessage").html('Vista ..');
				$.ajax({
					type: "POST",
					url: "/Exams/EditCheckbox/",
					data: { 
							option_id: this.id,
							correct: this.value
					 },
					contentType: "application/json; charset-utf8",
					dataType: "json",
					
					success: function(data) {
						setTimeout(function() {
							$("#savingMessage").html('Vistað!');
							setTimeout(function() {
								console.log(data['id']);
								$("input:checkbox[id='" + data['id'] + "']").prop("checked", false);
								$('#CheckLoader-' + id).fadeOut('slow');
								$('input[name="option-' + id +'"]').css('background-color', 'green');
								$('input[name="option-' + id +'"]').animate({ 'background-color' : 'white'}, 2000);
								//$('#Option-' + id).css('background-image', 'none')
								$('#Option-' + id).css('background-color', 'green');
								$('#Option-' + id).animate({'background-color' : 'white'}, 1);
								$('#Option-' + id).css( 'background-image', "url('/static/img/pw_maze_white.png') !important");
								$('#titleLoader').fadeOut('slow');
								$("#savingMessage").html("");
							}, 1000);
						}, 1500);

					},
					error: function() {

						$('CheckLoader-' + id).fadeOut('slow');
						$('#titleLoader').fadeOut('slow');
						$("#savingMessage").html("");
					}
				});		
			}
		}, 'input[type=checkbox]'
	);




function EditExamData(inputid, wheelid) {
	$("#savingMessage").html('Vista ..');
	$.ajax({
		type: "POST",
		url: "/Exams/EditExamData/",
		data: { exam_id: $('#examId').val(),
				name: $('#examName').val(),
				creation_date: $("#examCreationDate").val(),
				due_date: $("#examDueDate").val(),
                video_url: $('#examVideo').val()
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				$("#savingMessage").html('Vistað!');
				$(inputid).css('background-color', '#7AE969');
				$(inputid).animate({ 'background-color' : 'white'}, 2000);
				setTimeout(function() {
					$(wheelid).fadeOut('slow');
					$('#titleLoader').fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {

			$(wheelid).toggle();
		}
	})
}

function EditQuestion(inputid) {
	$("#savingMessage").html('Vista ..');
	$.ajax({
		type: "POST",
		url: "/Exams/EditQuestion/",
		data: { 
				question: $('#' + inputid).val(),
				question_id: inputid,
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				console.log("djamm");
				$("#savingMessage").html('Vistað!');
				$('#' + inputid).css('background-color', '#7AE969');
				$('#' + inputid).animate({ 'background-color' : 'white'}, 2000);
				setTimeout(function() {
					$('#Loader' + inputid).fadeOut('slow');
					$('#titleLoader').fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {

			$('Loader' + inputid).toggle();
			$('#titleLoader').fadeOut('slow');
			$("#savingMessage").html("");
		}
	})
}

function DeleteQuestion(inputid) {
	$("#savingMessage").html('Eyði ..');
	$.ajax({
		type: "POST",
		url: "/Exams/DeleteQuestion/",
		data: { 
				question_id: inputid,
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				$("#savingMessage").html('Eytt!');
				setTimeout(function() {
					$('#Question-' + inputid).fadeOut('slow');
					$('#titleLoader').fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {
			$('#Question-' + inputid).css('background-color', 'red');
			$('#Question-' + inputid).animate({ 'background-color' : 'white'}, 2000);
			$('#' + inputid).css('background-color', 'red');
			$('#' + inputid).animate({ 'background-color' : 'white'}, 2000);
			$('Loader' + inputid).toggle();
			$('#titleLoader').fadeOut('slow');
			$("#savingMessage").html("");
		}
	})
}

function EditOption(inputid) {
	$("#savingMessage").html('Vista ..');
	$.ajax({
		type: "POST",
		url: "/Exams/EditOption/",
		data: { 
				option: $('input[name="option-' + inputid +'"]').val(),
				option_id: inputid,
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				$("#savingMessage").html('Vistað!');
				$('input[name="option-' + inputid +'"]').css('background-color', '#7AE969');
				$('input[name="option-' + inputid +'"]').animate({ 'background-color' : 'white'}, 2000);
				setTimeout(function() {
					$('#optionLoader-' + inputid).fadeOut('slow');
					$('#titleLoader').fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {

			$('optionLoader-' + inputid).toggle();
			$('#titleLoader').fadeOut('slow');
			$("#savingMessage").html("");
		}
	})
}

function DeleteOption(inputid) {
	$("#savingMessage").html('Eyði ..');
	$.ajax({
		type: "POST",
		url: "/Exams/DeleteOption/",
		data: { 
				option_id: inputid,
		 },
		contentType: "application/json; charset-utf8",
		dataType: "json",
		success: function() {
			setTimeout(function() {
				$("#savingMessage").html('Eytt!');
				setTimeout(function() {
					$('#Option-' + inputid).fadeOut('slow');
					$('#titleLoader').fadeOut('slow');
					$("#savingMessage").html("");
				}, 1000);
			}, 1500);

		},
		error: function() {
			$('input[name="option-' + inputid +'"]').css('background-color', 'red');
			$('input[name="option-' + inputid +'"]').animate({ 'background-color' : 'white'}, 2000);
			$('#Option-' + inputid).css('background-image', 'none')
			$('#Option-' + inputid).css('background-color', 'red');
			$('#Option-' + inputid).animate({'background-color' : 'white'}, 2000);
			
			$('Loader' + inputid).toggle();
			$('#titleLoader').fadeOut('slow');
			$("#savingMessage").html("");
		}
	}).complete(function() {
		setTimeout(function() {
			$('#Option-' + inputid).css( 'background-image', "url('/static/img/pw_maze_white.png')");
		}, 2000);
	});

}


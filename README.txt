Nantischool!

�a� er einn kennari � kerfinu, eins og be�i� var um, notandinn dabs me� pass gaman82.

Svo eru fj�rir nemendur, �eir hafa allir sama lykilor� og �eir hafa notendanafn. Kennari getur s�� notendurnar �egar hann loggar sig inn og fer � notendaumsj�n.

Pr�fin sty�ja HTML5 video elementi�, eina sem �arf a� gera er a� gefa upp sl�� � mp4 skr� sem � a� l�ta fylgja. Hugsunin var a� kennari g�ti l�ti� myndband, svosem fyrirlestur, fylgja pr�finu. 
Video elementi� virkar vel � tveimur Windows t�lvum (og meira a� segja � android s�ma) � Chrome og IE, en af einhverri �st��u virkar �a� ekki � chromium n� opera � Linux v�l.
from django.http import HttpResponse
from django.shortcuts import render, redirect, Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from Exams.models import *
import datetime
import simplejson as json

@login_required
def create(request):
	if request.method == "GET":
		#Create default exam.
		exam = Exam(name="'Unnamed exam'",
					course="No course specified",
					created_by=request.user,
					due_date_time=datetime.date.today() + datetime.timedelta(days=7))
		exam.save()
		#Formatting the time according to the html5 date input standard.
		#browser decides how to display the string in the input element.
		exam.creation_date = exam.creation_date.strftime("%Y-%m-%d") 
		exam.due_date_time = exam.due_date_time.strftime("%Y-%m-%d")
		return render(request, 'Exams/create.html', {'exam' : exam })
	else:
		raise Http404

@login_required
def take(request, exam_id):
	if request.method == "GET":
		exam = Exam.objects.get(id=exam_id)
		mtm = StudentTakesExam.objects.get_or_create(exam=exam, student=request.user)
		if mtm[0].finished:
			return render(request, 'Exams/take.html', {'finished' : True })
		elif exam.due_date_time < datetime.date.today():
			return render(request, 'Exams/take.html', {'expired' : True })
		else:
			return render(request, 'Exams/take.html', {'exam' : exam, 'takes_exam' : mtm[0] })

	else:#if POST
		exam = Exam.objects.get(id=exam_id)
		mtm = StudentTakesExam.objects.get(student=request.user, exam=exam)
		mtm.finished = datetime.date.today()
		mtm.save()
		return redirect(list)


@login_required
def edit(request, exam_id):
	if request.method == "GET":
		#make sure user is a teacher.
		if request.user.userprofile_set.get().is_teacher:
			exam = Exam.objects.get(id=exam_id)
			exam.creation_date = exam.creation_date.strftime("%Y-%m-%d") 
			exam.due_date_time = exam.due_date_time.strftime("%Y-%m-%d")
			questions = None
			try:
				questions = Question.objects.filter(exam=exam)
			except Exception, e:
				print e
				#If the exam doesn't have any questions then questions is set to None
				pass
			return render(request, 'Exams/edit.html', {'exam' :  exam, 'questions' : questions })
	else:
		#No POST method, or student trying to edit exam.
		raise Http404

@login_required
def delete(request, exam_id):
	if request.method == "GET":
		#make sure user is a teacher.
		if request.user.userprofile_set.get().is_teacher:
			exam = Exam.objects.get(id=exam_id)
			#Django makes sure all the related objects (questions, options) get deleted as well.
			exam.delete()
			return redirect(list)

@login_required
def list(request):
	#Fetch all exams and order them from newest first.
	exam_list = Exam.objects.all().order_by('due_date_time')
	for e in exam_list:
		try:
			correct_count = 0

			#Calculate the score of the student for each exam.
			taken = StudentTakesExam.objects.get(student=request.user, exam=e)
			if taken.finished is None:
				e.grade = None
			else:
				questions_answered = taken.studentanswersquestion_set.all()
				for answer in questions_answered:
					if answer.option.correct:
						correct_count += 1
				e.correct = correct_count
				e.grade = float(correct_count) / e.question_set.all().count() * 10

			# Get every student that has taken the exam and calculate his score
			students_taken_exam = e.studenttakesexam_set.filter(exam=e).exclude(finished=None)
			grade_list = []
			for taken_exam in students_taken_exam:
				correct_count = 0
				taken = StudentTakesExam.objects.get(student=taken_exam.student, exam=e)
				questions_answered = taken.studentanswersquestion_set.all()
				for answer in questions_answered:
					if answer.option.correct:
						correct_count += 1
				grade = float(correct_count) / e.question_set.all().count() * 10
				grade_list.append(grade)
			grade_list.sort()
			grade_list.reverse()
			# Find the position the users grade is in the score list
			e.position = grade_list.index(e.grade) + 1
			e.exam_count = len(grade_list)
			e.taken = taken.finished
			# if taken.finished:
			# 	e.taken = True
			# else:
			# 	e.taken = False
		except Exception, ex:
			print ex
			e.taken = False
			# If we get to this exception, that is because the user has not taken the exam
			# But we still want to know how many other users have taken it
			e.exam_count = len(e.studenttakesexam_set.filter(exam=e).exclude(finished=None))
	return render(request, 'Exams/list.html', {'exam_list' : exam_list })


#AJAX FUNCTIONS
@csrf_exempt
def answer(request):
	if request.method == "POST":
		if request.is_ajax():
			option = Option.objects.get(id=request.POST['option_id'])
			mtm = StudentTakesExam.objects.get(id=request.POST['student_takes'])
			question = Question.objects.get(id=request.POST['question_id'])
			#if the student is changing his answer
			try:
				answer = StudentAnswersQuestion.objects.get(takes_exam=mtm, question=question)
				answer.option = option
				print answer.id
				answer.save()
				print answer.id
			#If student is answering for the first time	
			except:
				answer = StudentAnswersQuestion(takes_exam=mtm, question=question, option=option)
				answer.save()
			re = {'id' : '1'}
			return HttpResponse(json.dumps(re), status=200, mimetype='application/json')


@csrf_exempt
def edit_exam_data(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				e = Exam.objects.get(id=request.POST['exam_id'])
				e.name = request.POST['name']
				e.creation_date = request.POST['creation_date']
				e.due_date_time = request.POST['due_date']
				e.video = request.POST['video_url']
				e.save()
				re = {'id' : '1' }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

@csrf_exempt
def add_question(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				q = Question()
				q.exam = Exam.objects.get(id=request.POST['exam_id'])
				q.save()
				res = { 'id' : q.id }
				return HttpResponse(json.dumps(res), mimetype="application/json")
			except Exception, e:
				print e
				return HttpResponse(status=500)
	else:
		raise Http404

@csrf_exempt
def edit_question(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				q = Question.objects.get(id=request.POST['question_id'])
				q.question = request.POST['question']
				q.save()
				re = {'id' : q.id }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

@csrf_exempt
def delete_question(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				q = Question.objects.get(id=request.POST['question_id'])
				q.delete()
				re = {'id' : q.id }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

@csrf_exempt
def add_option(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				o = Option()
				o.question = Question.objects.get(id=request.POST['question_id'])
				o.save()
				res = { 'id' : o.id }
				return HttpResponse(json.dumps(res), mimetype="application/json")
			except Exception, e:
				print e
				return HttpResponse(status=500)
	else:
		raise Http404

@csrf_exempt
def edit_option(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				o = Option.objects.get(id=request.POST['option_id'])
				o.option = request.POST['option']
				o.save()
				re = {'id' : o.id }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

@csrf_exempt
def delete_option(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				o = Option.objects.get(id=request.POST['option_id'])
				o.delete()
				re = {'id' : o.id }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

@csrf_exempt
def edit_checkbox(request):
	if request.method == "POST":
		if request.is_ajax():
			try:
				o = Option.objects.get(id=request.POST['option_id'])
				if str(request.POST['correct']) == "True":
					o.correct = False
				else:
					o.correct= True
				#Get the old correct option before we save the new one.
				try:
					old_o = Option.objects.get(question=o.question, correct=True)
					print old_o
					old_o.correct = False
					old_o.save()
				except:
					pass
				o.save()
				if old_o:
					re = {'id' : old_o.id }
				else:
					re = {'id' : -1 }
				return HttpResponse(json.dumps(re), status=200, mimetype='application/json')
			except Exception, ex:
				print ex
				return HttpResponse(status=500)

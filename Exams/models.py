from django.db import models
from django.contrib.auth.models import User

class Exam(models.Model):

	name = models.TextField()
	course = models.TextField()

	creation_date = models.DateField(auto_now_add=True)
	due_date_time = models.DateField(null=False)

	video = models.URLField(max_length=1000, null=True)

	value = models.IntegerField(default=0)
	created_by = models.ForeignKey(User)

class Question(models.Model):

	question = models.TextField(null=False)
	exam = models.ForeignKey(Exam)


class Option(models.Model):

	option = models.TextField(null=False)
	correct = models.BooleanField(default=False)
	question = models.ForeignKey(Question)


class StudentTakesExam(models.Model):
	
	student = models.ForeignKey(User)
	exam = models.ForeignKey(Exam)
	started = models.DateField(auto_now_add=True)
	finished = models.DateField(default=None, null=True)

class StudentAnswersQuestion(models.Model):

	takes_exam = models.ForeignKey(StudentTakesExam)
	option = models.ForeignKey(Option)
	question = models.ForeignKey(Question)




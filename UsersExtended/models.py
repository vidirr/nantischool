from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):

	is_teacher = models.BooleanField(default=False)
	exams_completed = models.IntegerField(default=0)

	user = models.ForeignKey(User)

	def __unicode__(self):
		return "UserProfile for: " + self.user.username


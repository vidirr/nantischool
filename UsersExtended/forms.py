from django import forms

class CreateUserForm(forms.Form):
    name = forms.CharField(required=True)
    username = forms.CharField(required=True)
    password = forms.PasswordInput()
    email = forms.EmailField(required=True)
    is_teacher = forms.BooleanField()

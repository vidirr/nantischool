# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.template import Context
import django.contrib.messages as messages
from NantiSchool import views as NantiSchoolviews
from UsersExtended.models import UserProfile

def login_custom(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        #django auth module authenticate
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                #django auth module login
                login(request, user)
                print user
                return redirect(NantiSchoolviews.home)
        else:
            error = "Unable to log you in. Try again!"
            return render(request, 'UsersExtended/login.html', {'error' : error })
    else:
        return render(request, 'UsersExtended/login.html')

@login_required
def logout_custom(request):
    #django auth module logout.
    logout(request)
    return redirect(login_custom)

def register(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = User.objects.create_user(username, '', password)
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect(NantiSchoolviews.home)
        except Exception:
            pass
        error = "Something went wrong. Sawry."
        return render(request, 'UsersExtended/register.html',  {'error' : error })

@login_required
def adduser(request):
    if request.method == "POST":
        username = request.POST['username']
        if len(username) == 0:
            messages.error(request, "Notendanafn vantaði")
            return render(request, 'UsersExtended/adduser.html')
        password = request.POST['password']
        if len(password) == 0:
            messages.error(request, "Lykilorð vantaði")
            return render(request, 'UsersExtended/adduser.html')
        email = request.POST['email']
        if len(email) == 0:
            messages.error(request, 'Email vantaði')
            return render(request, 'UsersExtended/adduser.html')
        firstname = request.POST['firstname']
        if len(firstname) == 0:
            messages.error(request, 'Nafn vantaði')
            return render(request, 'UsersExtended/adduser.html')
        try:
            is_teacher = request.POST['teacher']
        except Exception:
            is_teacher = False

        #
        # is_teacher = request.POST['teacher']
        try:
            user = User.objects.create_user(username, email, password)
            user.first_name = firstname
            user.save()

            userProfile = UserProfile()
            userProfile.user = user
            userProfile.is_teacher = is_teacher
            userProfile.save()
            messages.success(request, "Notendi hefur verið búinn til")
            return redirect(listusers)
            # return render(request, 'UsersExtended/adduser.html', { 'msg' : "Notandi hefur verið búinn til"})
        except Exception, e:
            if (e.message.find("username")):
                messages.error(request, "Það er til notandi með þetta notendanafn")
            else:
                messages.error(request, "Villa kom upp við að búa til notanda: " + e.message)
            pass

    return render(request, 'UsersExtended/adduser.html')

@login_required
def listusers(request):
    users = User.objects.all()

    for user in users:
        profile = UserProfile.objects.get(id = user.id)
        user.profile = profile

    cont = Context({"users" : users})
    return render(request, 'UsersExtended/listusers.html', cont)

@login_required
def edituser(request, user_id):
    user = User.objects.get(id = user_id)
    user.profile = UserProfile.objects.get(id = user_id)
    return render(request, 'UsersExtended/edituser.html', { 'user' : user})

@login_required
def updateuser(request):
    if request.method == "POST":
        try:
            user_id = request.POST['user_id']
            user = User.objects.get(id=user_id)
            user.first_name = request.POST['firstname']
            user.email = request.POST['email']
            passw = request.POST['password']
            if len(passw) > 0:
                user.set_password(passw)
            user.save()


            userProfile = UserProfile.objects.get(id=user_id)
            #Semi hackish.. if the teacher checkbox isn't set, it doesn't seem to be included in the form post
            try:
                is_teacher = request.POST['teacher']
            except Exception:
                is_teacher = False

            userProfile.is_teacher = is_teacher
            userProfile.save()

            return redirect(listusers)
        except Exception, e:
            return render(request, 'UsersExtended/edituser.html', { 'user' : user, 'msg' : "Villa kom upp: " + e.message})

@login_required
def deluser(request, user_id):
    user = User.objects.get(id=user_id)
    user.delete()
    messages.success(request, "Notanda hefur verið eytt")
    return redirect(listusers)